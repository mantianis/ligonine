#!/usr/bin/python
# -*- coding: utf-8 -*-
import MySQLdb
import random, string, os
from faker import Faker
fakegen = Faker()
ip = os.environ['PRIVATEIP']
db = MySQLdb.connect(ip,"ligonine","ligonine","ligonine", charset='utf8')
cursor = db.cursor()
spec = [u'Akušerė', 'Bendrosios praktikos gydytojas', 'Odontologas', 'Psichiatras', u'Šeimos gydytojas', 'Chirurgas']
lvl = [1,2]
sect = [u'Vilniaus rajono Kalvelių ambulatorija', u'Vilniaus rajono Juodšilių ambulatorija', u'Vilniaus rajono Lavoriškių ambulatorija', u'Vilniaus rajono Nemėžio ambulatorija',
         u'Vilniaus rajono Pagirių ambulatorija', u'Sudervės BPG kabinetas']
start_time = ['07:00', '08:00', '09:00', '10:00', '11:00']
end_time = ['16:00', '17:00', '18:00', '19:00', '20:00']

sql_count = "SELECT count(*) FROM doctor_doctor"
cursor.execute(sql_count)
res = cursor.fetchone()
total_rows = res[0]

for i in range(total_rows + 1, total_rows + 20):
    name = fakegen.first_name() + " " + fakegen.last_name()
    specialist = spec[random.randint(0,5)]
    section = sect[random.randint(0,5)]
    cabinet = random.randint(100,200)
    email = fakegen.email()
    telephone_number = '86' + (''.join([random.choice(string.digits) for j in range(0, 7)]))
    level = lvl[random.randint(0,1)]
    monday = start_time[random.randint(0,4)] + ' - ' + end_time[random.randint(0,4)]
    tuesday = start_time[random.randint(0,4)] + ' - ' + end_time[random.randint(0,4)]
    wednesday = start_time[random.randint(0,4)] + ' - ' + end_time[random.randint(0,4)]
    thursday = start_time[random.randint(0,4)] + ' - ' + end_time[random.randint(0,4)]
    friday = start_time[random.randint(0,4)] + ' - ' + end_time[random.randint(0,4)]
    sunday = '-'
    saturday = '-'
    sql = "insert into doctor_doctor VALUES(%d, '%s', '%s', '%s', %d, '%s', '%s',%d, '%s', '%s','%s','%s','%s','%s','%s')" % \
      (i, name, specialist, section, cabinet, email, telephone_number, level, friday, monday, saturday, sunday, thursday, tuesday, wednesday)
    cursor.execute(sql)
    number_of_rows = db.commit()
db.close()
