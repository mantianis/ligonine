from django.shortcuts import render, redirect, reverse
from django.core.urlresolvers import reverse_lazy
from . import forms
from django.views.generic import CreateView, ListView, DeleteView
from doctor.models import PacientRegister
from django.contrib.auth.mixins import LoginRequiredMixin


class SignUp(CreateView):
  form_class = forms.UserCreateForm
  success_url = reverse_lazy('registration:login')
  template_name = 'registration/signup.html'


class ListUsersRegistrations(LoginRequiredMixin, ListView):
  model = PacientRegister
  context_object_name = 'registration_list'
  login_url = 'registration:login'

  def get_queryset(self):
    object_list = self.model.objects.filter(user_id=self.kwargs['pk'])
    return object_list


class RegistrationDeleteView(LoginRequiredMixin, DeleteView):
  model = PacientRegister
  success_url = reverse_lazy('index')
