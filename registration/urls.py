from django.conf.urls import url
from django.contrib.auth import views as auth_views # default login signout views from django
from . import views
from django.contrib.auth.views import (
    password_change,
)

app_name = 'registration'

urlpatterns = [
    url(r'^login/$', auth_views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url(r'^signup/$', views.SignUp.as_view(), name='signup'),
    url(r'^(?P<pk>\d+)/my-registrations/$', views.ListUsersRegistrations.as_view(template_name='registration/registration_list.html'), name='registrations'),
    url(r'^(?P<pk>\d+)/remove/$', views.RegistrationDeleteView.as_view(template_name='registration/registration_remove.html'), name='registration_remove'),
    url(r'^password/change/$', password_change, {'template_name': 'registration/password_change.html','post_change_redirect': 'registration:login'}, name='password_change'),
]
