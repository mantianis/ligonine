from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.db import models

class MyUserManager(BaseUserManager):
    def create_superuser(self, password, email):

        user = self.model(
            email=self.normalize_email(email),
        )
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.last_name = 'admin'
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser, PermissionsMixin):

    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=60, unique=True)
    personal_code = models.CharField(max_length=30)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    objects = MyUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.last_name

    def get_short_name(self):
        return self.last_name
