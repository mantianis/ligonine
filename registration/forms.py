from registration.models import MyUser
# from django.contrib.auth.models import User
from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
import random
import string
# import smtplib
from django.core.mail import EmailMessage
from django.core.mail import send_mail

class UserCreateForm(forms.ModelForm):
    class Meta:
        fields = ('personal_code', 'last_name', 'email',)
        model = MyUser

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args,**kwargs)

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        default_password = (''.join([random.choice(string.digits + string.ascii_lowercase) for i in range(0, 10)]))
        user.set_password(default_password)
        if commit:
            user.save()
            message = 'Your password is: {}'.format(default_password)
            email = EmailMessage('Password', message, to=[user.email])
            email.send()
        return user
