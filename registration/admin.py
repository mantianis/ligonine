from django.contrib import admin
from . import models
# Register your models here.


class MyAdmin(admin.ModelAdmin):
    fields = ['last_name','personal_code', 'email',]

admin.site.register(models.MyUser, MyAdmin)
