from django import forms
from .models import PacientRegister

class RegisterForm(forms.ModelForm):
  CHOICES = (('00', '00'),('10', '10'),('20', '20'),('30', '30'),('40', '40'),('50', '50'),)
  CHOICES1 = (('07', '07'),('08', '08'),('09', '09'),('10', '10'),('11', '11'),('12', '12'),
             ('13', '13'),('14', '14'),('15', '15'),('16', '16'),('17', '17'),('18', '18'),
             ('19', '19'),)
  minutes = forms.ChoiceField(choices=CHOICES)
  hour = forms.ChoiceField(choices=CHOICES1)
  class Meta:
    model = PacientRegister
    fields = ('date', 'hour', 'minutes',)
    widgets = {
            'date': forms.DateTimeInput(attrs={'id': 'datepicker'}),
        }
