from django.db import models
# Create your models here.
from registration.models import MyUser
class Doctor(models.Model):
    name = models.CharField(max_length=140, unique=True)
    specialist = models.CharField(max_length=50)
    section = models.CharField(max_length=100)
    cabinet = models.IntegerField()
    email = models.EmailField(max_length=60)
    telephone_number = models.CharField(max_length=50)
    level = models.CharField(max_length=10)
    monday = models.CharField(max_length=20, null=True)
    tuesday = models.CharField(max_length=20, null=True)
    wednesday = models.CharField(max_length=20, null=True)
    thursday = models.CharField(max_length=20, null=True)
    friday = models.CharField(max_length=20, null=True)
    saturday = models.CharField(max_length=10, null=True)
    sunday = models.CharField(max_length=10, null=True)

    def __str__(self):
        return self.name


class PacientRegister(models.Model):
    doctor = models.ForeignKey(Doctor, related_name='register')
    user = models.ForeignKey(MyUser, related_name='register')
    date = models.CharField(max_length=100, null=True)
    class Meta:
        unique_together = ('doctor', 'date',)
