# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView
from doctor.models import Doctor, PacientRegister
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import RegisterForm
from django.core.urlresolvers import reverse_lazy
from django.db import IntegrityError
from django.http import request
import datetime

# Create your views here.
class RegisterView(LoginRequiredMixin, CreateView):
  model = PacientRegister
  form_class = RegisterForm
  success_url = reverse_lazy('index')

  def form_valid(self, form):
    try:
      self.object = form.save(commit=False)
      doc = Doctor.objects.get(pk=self.kwargs['pk'])
      day = self.request.POST.get('date')
      hour = self.request.POST.get('hour')
      minutes = self.request.POST.get('minutes')
      today = datetime.datetime.today().strftime('%m/%d/%Y')
      # Ar gera diena?
      if(int(day[-4:]) <= int(today[-4:])):
        if(today >= day):
          context = {'form': form, 'errmsg': u'Galite registruotis tik ateinančioms dienoms'}
          return render(self.request, 'doctor/register.html', context)
      week_day = datetime.datetime.strptime(self.request.POST.get('date'), '%m/%d/%Y').strftime('%A').lower()
      if(week_day == 'sunday' or week_day == 'saturday'):
        context = {'form': form, 'errmsg': 'Savaitgaliais daktarai nedirba'}
        return render(self.request, 'doctor/register.html', context)
      # Ar daktaras dirba tom valandom ?
      work_time = doc.__dict__[week_day]
      doc_start_hour = work_time[:2]
      doc_end_hour = work_time[8:10]
      if(doc_start_hour > hour or hour >= doc_end_hour):
        context = {'form': form, 'errmsg': u'Daktaras šiuo metu, šia dieną nedirba.'}
        return render(self.request, 'doctor/register.html', context)
      # Jei viskas gerai:
      self.object.user = self.request.user
      self.object.doctor = doc
      self.object.date = day + ' ' + hour + ':' + minutes
      self.object.save()
      return super(RegisterView, self).form_valid(form)
    except IntegrityError:
      context = {'form': form, 'errmsg': u'Laikas užimtas'}
      return render(self.request, 'doctor/register.html', context)


class ListDoctors(ListView):
  model = Doctor

  def get_queryset(self):
    name = self.request.GET.get('name')
    if (name == None):
      object_list = self.model.objects.all()
    else:
      object_list = self.model.objects.filter(name__icontains=name)
    return object_list


class ListFirstLevelDoctors(LoginRequiredMixin, ListView):
  model = Doctor
  login_url = 'registration:login'

  def get_queryset(self):
    object_list = self.model.objects.filter(level=1)
    return object_list


class ListSecondLevelDoctors(LoginRequiredMixin, ListView):
  model = Doctor
  login_url = 'registration:login'


  def get_queryset(self):
    object_list = self.model.objects.filter(level=2)
    return object_list


class DoctorDetails(DetailView):
  model = Doctor

