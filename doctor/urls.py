from django.conf.urls import url
from doctor import views

app_name = 'doctor'

urlpatterns = [
    url(r'^list/$', views.ListDoctors.as_view(template_name='doctor/list_doctors.html'), name='list_doctors'),
    url(r'^(?P<pk>\d+)/$', views.DoctorDetails.as_view(template_name='doctor/doctor_details.html'), name='doctor_details'),
    url(r'^list/first-level/$', views.ListFirstLevelDoctors.as_view(template_name='doctor/list_first_doctors.html'), name='list_first_doctors'),
    url(r'^list/second-level/$', views.ListSecondLevelDoctors.as_view(template_name='doctor/list_second_doctors.html'), name='list_second_doctors'),
    url(r'^register/(?P<pk>\d+)/$', views.RegisterView.as_view(template_name='doctor/register.html'), name='register'),
]
